;;;
;;; compile-stub.scm - inline-stub compiler
;;;
;;;   Copyright (c) 2016 KOGURO, Naoki (naoki@koguro.net)
;;;   All rights reserved.
;;;
;;;   Redistribution and use in source and binary forms, with or without
;;;   modification, are permitted provided that the following conditions
;;;   are met:
;;;
;;;   1. Redistributions of source code must retain the above copyright
;;;      notice, this list of conditions and the following disclaimer.
;;;   2. Redistributions in binary form must reproduce the above copyright
;;;      notice, this list of conditions and the following disclaimer in the
;;;      documentation and/or other materials provided with the distribution.
;;;   3. Neither the name of the authors nor the names of its contributors
;;;      may be used to endorse or promote products derived from this
;;;      software without specific prior written permission.
;;;
;;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
;;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
;;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;

(define-module compile-stub
  (use file.util)
  (use gauche.config)
  (use gauche.parseopt)
  (use gauche.process)
  (use rfc.sha)
  (use srfi-1)
  (use srfi-13)
  (use srfi-98)
  (use text.tr)
  (use util.list)
  (use util.match)

  (export inline-stub
          compile-stub
          define-cfn
          define-cproc
          define-enum
          define-enum-conditionally))

(select-module compile-stub)

(load "compile-stub-config.scm")

(define *module-stub-snippets-table* (make-hash-table 'eq?))

(define *compiled-binary-arch-directory* (apply build-path
                                                (expand-path COMPILED-BINARY-PATH)
                                                (take-right (string-split (gauche-config "--pkgarchdir")
                                                                          (path-separator))
                                                            3)))

(define (register-stub-snippets! name snippets)
  (hash-table-push! *module-stub-snippets-table* name snippets))

(define (has-snippets? name)
  (hash-table-exists? *module-stub-snippets-table* name))

(define (retrieve-snippets! name)
  (apply append (reverse (hash-table-get *module-stub-snippets-table* name '()))))

(define (remove-snippets! name)
  (hash-table-delete! *module-stub-snippets-table* name))

(define (compute-digest name snippets options)
  (define (write-snippet snippet out)
    (for-each (lambda (sexpr)
                (match sexpr
                  (('include file)
                   ;; TODO: find the file in search path if include supports the search path.
                   (unless (file-exists? file)
                     (error "couldn't find include file: " file))
                   (write-snippet (call-with-input-file file port->sexp-list) out))
                  (_
                   (write sexpr out))))
              snippet))

  (digest-hexify (sha1-digest-string (call-with-output-string
                                       (lambda (out)
                                         (write name out)
                                         (for-each (cut write-snippet <> out) snippets)
                                         (write (sort options
                                                      (lambda (p0 p1)
                                                        (string<? (car p0) (car p1))))
                                                out))))))

(define (stub-extension name options)
  (let1 snippets (hash-table-get *module-stub-snippets-table* name '())
    (string-append "x" (string-downcase (compute-digest name snippets options)))))

(define (skip-line out current-line-num to-line-num)
  (dotimes (_ (max 0 (- to-line-num current-line-num)))
    (newline out))
  to-line-num)

(define (write-sexpr-with-preserving-line-number sexpr out current-line-num)
  (let1 line-num (or (and-let* ((source-info (debug-source-info sexpr))
                                (source-line-num (second source-info)))
                       (skip-line out current-line-num source-line-num))
                     current-line-num)
    (cond
      ((list? sexpr)
       (display "(" out)
       (let loop ((rest sexpr)
                  (line-num line-num))
         (cond
           ((null? rest)
            (display ")" out)
            line-num)
           (else
            (loop (cdr rest)
                  (begin0
                    (write-sexpr-with-preserving-line-number (car rest) out line-num)
                    (unless (null? (cdr rest))
                      (display " " out))))))))
      ((pair? sexpr)
       (display "(" out)
       (begin0
         (write-sexpr-with-preserving-line-number (cdr sexpr) out
                                                  (begin0
                                                    (write-sexpr-with-preserving-line-number (car sexpr) out line-num)
                                                    (display " . " out)))
         (display ")" out)))
      (else
       (write sexpr out)
       line-num))))

(define (prepare-stublib-source stublib-src-filename name stub-ext)
  (let1 snippets (retrieve-snippets! name)
    (let ((stublib-name (path-sans-extension (sys-basename stublib-src-filename)))
          (line-num 1))
      (call-with-output-file stublib-src-filename
        (lambda (out)
          (for-each (lambda (snippet)
                      (set! line-num (+ (write-sexpr-with-preserving-line-number snippet out line-num) 1))
                      (newline out))
                    snippets)
          (write `(declcode
                   (.include "gauche.h" "gauche/extend.h"))
                 out)
          (write (format
                     (string-append
                       "static void Scm_Init_~a(ScmModule *mod);"
                       "SCM_EXTENSION_ENTRY void Scm_Init_~a() {"
                       "    ScmModule *mod;"
                       "    SCM_INIT_EXTENSION(~:*~a);"
                       "    mod = SCM_MODULE(SCM_FIND_MODULE(\"~a\", TRUE));"
                       "    Scm_Init_~0@*~a(mod);"
                       "}")
                   stublib-name
                   stub-ext
                   name)
                 out))))))

(define (options-alist->cmd-list options-alist)
  (map (match-lambda
        (("--verbose" . val)
         "--verbose")
        ((opt . val)
         (format "~a=~a" opt val)))
       options-alist))

(define (cmd->string cmd)
  (string-join (map shell-escape-string cmd) " "))

(define (compile-stublib stub-so-filename stub-ext stublib-src-filename options-alist stub-options)
  (let ((verbose? (memq 'verbose stub-options))
        (options (options-alist->cmd-list options-alist)))
    (let1 curdir (current-directory)
      (unwind-protect
       (begin
         (current-directory (sys-dirname stublib-src-filename))
         (let* ((log-filename (format "compile-output.~a.log"
                                (path-sans-extension (sys-basename stublib-src-filename))))
                (cmd `("gauche-package" "compile"
                       ,(format "--output=~a" (sys-basename stub-so-filename))
                       ,@options
                       ,stub-ext
                       ,(sys-basename stublib-src-filename)))
                (p (begin
                     (when verbose?
                       (with-output-to-file log-filename
                         (lambda ()
                           (print #"cd ~(sys-dirname stublib-src-filename)")
                           (print (cmd->string cmd)))))
                     (run-process cmd :wait #t :redirects `((>> 1 ,log-filename) (>& 2 1))))))
           (process-wait p)
           (let1 success? (= (sys-wait-exit-status (process-exit-status p)) 0)
             (unless (= (file-size log-filename) 0)
               (display (file->string log-filename) (current-error-port))
               (newline (current-error-port)))
             (unless success?
               (errorf "gauche-package compile exits with ~a" (sys-wait-exit-status (process-exit-status p)))))))
       (current-directory curdir)))))

(define (load-path-options)
  (let* ((default-load-paths (read-from-string (process-output->string '("gosh" "-e" "(write *load-path*)"))))
         (first-default-load-path (first default-load-paths))
         (last-default-load-path (last default-load-paths)))
    (define (collect-head-options load-paths head-options tail-options)
      (cond
        ((null? load-paths)
         (values head-options tail-options))
        ((equal? (car load-paths) first-default-load-path)
         (skip-load-paths load-paths head-options tail-options))
        (else
         (collect-head-options (cdr load-paths) (cons (format "-I~a" (car load-paths)) head-options) tail-options))))
    (define (skip-load-paths load-paths head-options tail-options)
      (cond
        ((null? load-paths)
         (values head-options tail-options))
        ((equal? (car load-paths) last-default-load-path)
         (collect-tail-options (cdr load-paths) head-options tail-options))
        (else
         (skip-load-paths (cdr load-paths) head-options tail-options))))
    (define (collect-tail-options load-paths head-options tail-options)
      (cond
        ((null? load-paths)
         (values head-options tail-options))
        (else
         (collect-tail-options (cdr load-paths) head-options (cons (format "-A~a" (car load-paths)) tail-options)))))
    (receive (head-options tail-options) (collect-head-options *load-path* '() '())
      (append head-options (reverse tail-options)))))

(define (precompile-stub compile-options control-options)
  (let* ((this-filename (current-load-path))
         (out.c (path-swap-extension (sys-basename this-filename) "c"))
         (dso-basename (path-sans-extension out.c))
         (precomp-cmd `("gosh" ,@(load-path-options) "precomp" "-e" ,this-filename))
         (compile-cmd `("gauche-package" "compile" ,@(options-alist->cmd-list compile-options) ,dso-basename ,out.c))
         (verbose? (memq 'verbose control-options)))
    (when verbose?
      (print (cmd->string precomp-cmd)))
    (process-wait (run-process precomp-cmd :wait #t))
    (when verbose?
      (print (cmd->string compile-cmd)))
    (process-wait (run-process compile-cmd :wait #t))))

(define (clean-precompile-files control-options)
  (let* ((this-filename (current-load-path))
         (out.sci (path-swap-extension this-filename "sci"))
         (out.c (path-swap-extension (sys-basename this-filename) "c"))
         (out.o (path-swap-extension out.c (gauche-config "--object-suffix")))
         (dso-basename (path-sans-extension out.c))
         (compile-cmd `("gauche-package" "compile" "--clean" ,dso-basename))
         (clean-files (list out.sci out.c out.o))
         (verbose? (memq 'verbose control-options)))
    (when verbose?
      (print (cmd->string compile-cmd)))
    (process-wait (run-process compile-cmd :wait #t))
    (when verbose?
      (format (current-output-port) "rm -f ~a~%" (string-join clean-files " ")))
    (delete-files clean-files)))

(define (print-makefile kw-options pkg-config env-compile-options)
  (let* ((this-filename (current-load-path))
         (out.c (path-swap-extension (sys-basename this-filename) "c"))
         (out.sci (path-swap-extension (sys-basename this-filename) "sci"))
         (dso-basename (path-sans-extension out.c))
         (load-path-options-string (string-join (load-path-options) " "))
         (pkgs (if (list? pkg-config)
                   (string-join pkg-config " ")
                   pkg-config))
         (pkg-options (if pkgs
                          `(("--cflags" . ,#"$(shell pkg-config --cflags ~pkgs)")
                            ("--libs" . ,#"$(shell pkg-config --libs ~pkgs)"))
                          '()))
         (compile-cmd-options-string (string-join
                                      (options-alist->cmd-list
                                       (map (^p (cons (car p) (shell-escape-string (cdr p))))
                                            (merge-options (list kw-options
                                                                 env-compile-options
                                                                 pkg-options))))
                                      " ")))
    (with-output-to-port (current-output-port)
      (lambda ()
        (display #"SOEXT = $(shell gauche-config --so-suffix)\n")
        (display #"\n")
        (display #"~|dso-basename|.$(SOEXT): ~out.c\n")
        (display #"\tgauche-package compile ~compile-cmd-options-string ~dso-basename $<\n")
        (display #"\n")
        (display #"~out.sci ~|out.c|: ~this-filename\n")
        (display #"\tgosh ~load-path-options-string precomp -e $<\n")))))

(define (merge-options options-list)
  (match options-list
    ((opts)
     opts)
    ((opts0 opts1 . rest)
     (let1 opt-table (make-hash-table 'string=?)
       (for-each (match-lambda
                  ((k . v)
                   (hash-table-push! opt-table k v)))
                 (append opts0 opts1))
       (merge-options (cons (hash-table-map opt-table
                              (lambda (k vs)
                                (cons k (cond
                                          ((list? vs)
                                           (string-join vs (if (string=? k "--local")
                                                               ":"
                                                               " ")))
                                          (else
                                           vs)))))
                            rest))))))

(define (build-lib-dir modname file-sans-ext)
  (build-path *compiled-binary-arch-directory* (symbol->string modname) file-sans-ext))

(define (build-lock-name modname)
  (let* ((file-sans-ext (values-ref (decompose-path (or (current-load-path) "")) 1))
         (lock-dir (build-path (temporary-directory) "compile-stub" (symbol->string modname))))
    (create-directory* lock-dir)
    (build-path lock-dir (string-append file-sans-ext ".lock"))))

(define (%compile-stub name :key (local #f) (cc #f) (cppflags #f) (cflags #f) (ldflags #f) (libs #f) (pkg-config #f))
  (cond
    ((not name)
     ;; The current module is anonymous module.  We'll report an error in this case.
     ;; This situation can happen if the stub is defined in user module and the script is checked with check-script.
     (errorf "compile-stub doesn't support anonymous module."))
    (else
     (with-lock-file (build-lock-name name)
       (lambda ()
         (when (has-snippets? name)
           (let* ((kw-options (cond-list
                                (local `("--local" . ,local))
                                (cc `("--cc" . ,cc))
                                (cppflags `("--cppflags" . ,cppflags))
                                (cflags `("--cflags" . ,cflags))
                                (ldflags `("--ldflags" . ,ldflags))
                                (libs `("--libs" . ,libs))))
                  (pkg-options (cond
                                 ((string? pkg-config)
                                  (list (cons "--cflags" (process-output->string `("pkg-config" "--cflags" ,pkg-config)))
                                        (cons "--libs" (process-output->string `("pkg-config" "--libs" ,pkg-config)))))
                                 ((list? pkg-config)
                                  (list (cons "--cflags" (process-output->string `("pkg-config" "--cflags" ,@pkg-config)))
                                        (cons "--libs" (process-output->string `("pkg-config" "--libs" ,@pkg-config)))))
                                 (else
                                  '())))
                  (env-args (shell-tokenize-string (or (get-environment-variable "COMPILE_STUB_OPTIONS") "")))
                  (env-compile-options (let-args env-args
                                           ((verbose? "v|verbose" #f)
                                            (local "local=s" #f)
                                            (cppflags "cppflags=s" #f)
                                            (cflags "cflags=s" #f)
                                            (ldflags "ldflags=s" #f)
                                            (libs "libs=s" #f)
                                            (else (_ rest cont)
                                                  (cont rest)))
                                         (cond-list
                                           (verbose? `("--verbose" . ""))
                                           (local `("--local" . ,local))
                                           (cc `("--cc" . ,cc))
                                           (cppflags `("--cppflags" . ,cppflags))
                                           (cflags `("--cflags" . ,cflags))
                                           (ldflags `("--ldflags" . ,ldflags))
                                           (libs `("--libs" . ,libs)))))
                  (control-options (let-args env-args
                                       ((verbose? "v|verbose" #f)
                                        (force-compile? "force-compile" #f)
                                        (precompile? "precompile" #f)
                                        (clean? "clean" #f)
                                        (print-makefile? "print-makefile" #f)
                                        (else (_ rest cont)
                                              (cont rest)))
                                     (cond-list
                                       (verbose? 'verbose)
                                       (force-compile? 'force-compile)
                                       (precompile? 'precompile)
                                       (print-makefile? 'print-makefile)
                                       (clean? 'clean))))
                  (options-alist (merge-options (list kw-options pkg-options env-compile-options)))
                  (stublib-src-basename (values-ref (decompose-path (or (current-load-path) "")) 1))
                  (lib-dir (build-lib-dir name stublib-src-basename))
                  (stub-ext (stub-extension name options-alist))
                  (lib-ext-dir (build-path lib-dir stub-ext))
                  (stublib-src-filename (build-path lib-ext-dir (string-append stublib-src-basename ".stub")))
                  (stub-so-filename (build-path lib-ext-dir (string-append stub-ext "." (gauche-config "--so-suffix")))))
             (cond
               ((memq 'print-makefile control-options)
                (print-makefile kw-options pkg-config env-compile-options))
               ((and (memq 'precompile control-options)
                     (memq 'clean control-options))
                (clean-precompile-files control-options))
               ((memq 'precompile control-options)
                (precompile-stub options-alist control-options))
               (else
                (unwind-protect
                    (begin
                      (when (or (not (file-exists? stub-so-filename))
                                (memq 'force-compile control-options))
                        (when (file-is-directory? lib-dir)
                          (delete-directory* lib-dir))
                        (create-directory* lib-ext-dir)
                        (prepare-stublib-source stublib-src-filename name stub-ext)
                        (compile-stublib stub-so-filename
                                         stub-ext
                                         stublib-src-filename
                                         options-alist
                                         control-options))
                      (dynamic-load stub-so-filename))
                  (remove-snippets! name)))))))))))

(define-syntax inline-stub
  (er-macro-transformer
    (lambda (form rename id=?)
      (match form
        ((_ body ...)
         (quasirename rename
                      (begin
                        (eval-when (,':load-toplevel ,':execute)
                          (register-stub-snippets! (module-name (current-module)) ',body))
                        (eval-when (,':compile-toplevel)
                          (inline-stub ,@body)))))))))

(define-macro (redefine-stub-macros :rest macros)
  `(begin ,@(map (lambda (macro)
                   `(define-syntax ,macro
                      (er-macro-transformer
                        (lambda (form rename id=?)
                          (quasirename rename
                                       (begin
                                         (eval-when (,',':load-toplevel ,',':execute)
                                           (register-stub-snippets! (module-name (current-module)) '(,',form)))
                                         (eval-when (,',':compile-toplevel)
                                           ,',form)))))))
                 macros)))

(redefine-stub-macros define-cfn define-cproc define-enum define-enum-conditionally)

(define-syntax compile-stub
  (er-macro-transformer
    (lambda (form rename id=?)
      (match form
        ((_ options ...)
         (quasirename rename
                      (eval-when (,':load-toplevel ,':execute)
                        (%compile-stub (module-name (current-module)) ,@options))))))))
