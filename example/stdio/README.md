This directory contains an example script which uses stdio.

To run the script,

    % gosh -I. hello.scm [TARGET]

The script just says Hello, TARGET. The default value of TARGET is 'world'.
