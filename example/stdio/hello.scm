(use compile-stub)
(use gauche.parseopt)

;;;

(inline-stub
  (declcode
   (.include "stdio.h")))

(define-cproc say-hello-to (str::<const-cstring>)
  ::<void>
  (printf "Hello, %s.\n" str))

(compile-stub)

;;;

(define (main args)
  (let-args (cdr args)
      ((help "h|help" => (lambda _
                           (format (current-error-port) "Usage: ~a [-h|help] TARGET~%" (car args))
                           (exit 1)))
       . restargs)
    (let1 target (if (null? restargs)
                     "world"
                     (car restargs))
      (say-hello-to target)))
  0)

