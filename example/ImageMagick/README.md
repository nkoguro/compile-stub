MagickWand API and Precompile example
=====================================

This directory contains an example script which uses MagickWand API.

To run the script,

    % gosh -I. draw-mandelbrot.scm

Precompile
----------
You can precompile these modules by these commands.

    % env COMPILE_STUB_OPTIONS="--precompile" gosh -I. magick.scm
    % env COMPILE_STUB_OPTIONS="--precompile" gosh -I. mandelbrot.scm

After that, `gosh -I. draw-mandelbrot.scm` uses the precompiled modules. No dynamic compilation will not happen.

To clean the generated files (\*.sci and \*.so),

    % env COMPILE_STUB_OPTIONS="--precompile --clean" gosh -I. magick.scm
    % env COMPILE_STUB_OPTIONS="--precompile --clean" gosh -I. mandelbrot.scm
