;; Draw Mandelbrot set.

(use data.queue)
(use gauche.parseopt)
(use gauche.uvector)
(use magick)
(use mandelbrot)
(use srfi-11)

(define (main args)
  (let-args (cdr args)
      ((width "width=i" 640)
       (height "height=i" 480)
       (max-loop "loop=i" 100)
       (help "h|help" => (lambda _
                           (format (current-error-port) "Usage: ~a [--width=i --height=i --loop=i]~%" (car args))
                           (exit 1))))
    (let* ((pixels (make-f64vector (* width height 3) 0)))
      (mandelbrot-fill! width height pixels -0.7 0 4 max-loop)
      (let1 wand (make-image width height pixels)
        (display-image wand))))
  0)
