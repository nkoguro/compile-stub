(define-module mandelbrot
  (use compile-stub)

  (export mandelbrot-fill!))

(select-module mandelbrot)

(inline-stub
 (declcode
  (.include "gauche/vector.h"
            "math.h")))

(define-cfn v->red (v::double)
  ::double :static :inline
  (return (/ (+ (tanh (/ (* 3.0 (- (* 2.0 v) 1.2)) 2.0)) 1.0) 2.0)))

(define-cfn v->green (v::double)
  ::double :static :inline
  (return (/ (+ (tanh (/ (* 3.0 (- (* 2.0 v) 1.0)) 2.0)) 1.0) 2.0)))

(define-cfn v->blue (v::double)
  ::double :static :inline
  (return (/ (+ (tanh (/ (* 3.0 (- (* 2.0 v) 0.8)) 2.0)) 1.0) 2.0)))

(define-cfn put-color! (pixels::ScmF64Vector* index::int zr::double zi::double max-loop::int)
  ::void :static :inline
  (let* ((znr::double zr)
         (zni::double zi))
    (dotimes (i max-loop)
      (when (<= 4 (+ (* znr znr) (* zni zni)))
        (let* ((v::double (/ (cast double i) (cast double max-loop)))
               (red::double (v->red v))
               (green::double (v->green v))
               (blue::double (v->blue v)))
          (set! (SCM_F64VECTOR_ELEMENT pixels (+ index 0)) red
                (SCM_F64VECTOR_ELEMENT pixels (+ index 1)) green
                (SCM_F64VECTOR_ELEMENT pixels (+ index 2)) blue))
        (return))
      (let* ((znnr::double (+ (- (* znr znr) (* zni zni)) zr))
             (znni::double (+ (* 2 znr zni) zi)))
        (set! znr znnr
              zni znni)))
    (return)))

(define-cproc mandelbrot-fill! (width::<int>
                                height::<int>
                                pixels::<f64vector>
                                center-x::<double>
                                center-y::<double>
                                range-x::<double>
                                max-loop::<int>)
  (let* ((range-y::double (* range-x (/ (cast double height) (cast double width)))))
    (dotimes (y height)
      (dotimes (x width)
        (let* ((zr::double (+ (* range-x (- (/ (cast double x) width) 0.5)) center-x))
               (zi::double (+ (* range-y (- 0.5 (/ (cast double y) height))) center-y))
               (index::int (* 3 (+ (* y width) x))))
          (put-color! pixels index zr zi max-loop))))))

(compile-stub)
