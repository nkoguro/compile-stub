;; Simple image viewer using MagickWand API

(define-module magick
  (use compile-stub)

  (export make-image
          display-image))

(select-module magick)

(inline-stub
  (declcode
   (.include "gauche.h"
	     "gauche/vector.h"
	     "wand/MagickWand.h"))

  (define-cptr <magick-wand> :private
    "MagickWand*"                       ; c-type
    "MagickWandClass"                   ; c-name
    "MAGICK_WAND_P"                     ; c-pred
    "MAKE_MAGICK_WAND"                  ; c-boxer
    "MAGICK_WAND"                       ; c-unboxer
    (flags)
    (print #f)
    (cleanup "cleanup_magick_wand")
    )

  (define-cfn cleanup-magick-wand (z)
    ::void :static
    (when (MAGICK_WAND_P z)
      (DestroyMagickWand (MAGICK_WAND z))
      ;; Avoid unnecessary finalization
      (Scm_UnregisterFinalizer z)))

  (define-cfn teardown (data::void*)
    ::void
    (MagickWandTerminus))

  (initcode
   (MagickWandGenesis)
   (Scm_AddCleanupHandler teardown NULL)))

(define-cproc make-image (width::<int> height::<int> pixels::<f64vector>)
  ::<magick-wand>
  (let* ((wand::MagickWand* (NewMagickWand)))
    (unless (MagickConstituteImage wand width height "RGB" DoublePixel (SCM_F64VECTOR_ELEMENTS pixels))
      (Scm_Error "MagickConstituteImage failed."))
    (return wand)))

(define-cproc display-image (wand::<magick-wand>)
  ::<void>
  (unless (MagickDisplayImage wand "")
    (Scm_Error "MagickDisplayImage failed.")))

(compile-stub :pkg-config "MagickWand")

