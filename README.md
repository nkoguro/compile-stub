compile-stub - On-demand inline-stub Compiler
=============================================

This module introduces inline-stub on-demand compilation feature. You can embed
CiSE code in your module with inline-stub and the CiSE code will be compiled
dynamically when it is loaded.


Install
-------

    % ./configure
    % make
    # make install

The configure script can take --with-compiled-binary-path to change the path to store compiled binaries.
The default value is `XDG_CACHE_HOME` or "~/.cache/compiled-stub" if the environment variable is undefined.


How to Use
----------
1. Add `(use compile-stub)` in your script.
2. Write CiSE code in inline-stub. Like `(inline-stub stub-form ...)`.
3. Add `(compile-stub args ...)` in the end of the script.
4. Let's run the script. The stub code will be compiled at run-time.

There are some example codes under example/.


Macros
------
compile-stub module provides these macros.


* Macro: (**inline-stub** _stub-form ..._)
    - Embed stub code. It will be compiled to native binary by compile-stub.

* Macro: (**define-cproc** _name (args ...) \[rettype\] \[flag ...\] \[qual ...\] body ..._)
    - Create a subr function.

* Macro: (**define-enum** _name_)
    - Define a constant for the enum value.

* Macro: (**define-enum-conditionally** _name_)
    - Define a constant for the enum value if it exists.

* Macro: (**compile-stub** _:key local cc cppflags cflags ldflags libs pkg-config_)
    - Compile stub code. The compiled binary will be recorded under ~/.local/lib/compile-stub/gauche-<ver>/site/<arch>. If the stub code is unchanged, the previous compiled binary will be reused. `compile-stub` takes these keywords.
        + :local "PATH:PATH..."
            - Specify local paths. They are used to include header and library search paths.
        + :cc "CC"
            - Specify alternative C compiler.
        + :cppflags "CPPFLAGS"
            - Specify cpp flags.
        + :cflags "CFLAGS"
            - Specify cc flags.
        + :ldflags "LDFLAGS"
            - Specify ld flags.
        + :libs "LIBS"
            - Specify libraries.
        + :pkg-config "LIBRARY" or ("LIBRARY1" "LIBRARY2" ...)
            - Specify library names. :cflags and :libs are automatically set by pkg-config(1).


Environment
-----------
You can control compile-stub behavior with `COMPILE_STUB_OPTIONS` environment
variable. The variable can take these options:

* --cc=CC
    - Specify alternative C compiler.
* --cppflags=CPPFLAGS
    - Specify additional cpp flags.
* --cflags=CFLAGS
    - Specify additional cc flags.
* --clean
    - Remove the intermediate and output files of precompile. This option takes effect only when --precompile is specified.
* --force-compile
    - Force to compile even if inline-stub hasn't been changed.
* --ldflags=LDFLAGS
    - Specify additional ld flags.
* --libs=LIBRARIES
    - Specify additional libraries.
* --local=PATH:PATH:...
    - Specify additional local paths.
* --precompile
    - Compile the module itself. This option generates sci and dso files.
* --print-makefile
    - Print Makefile into the current output stream.
* --verbose
    - Report commands being executed.
