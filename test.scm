;;;
;;; Test compile-stub
;;;

(use gauche.test)

(test-start "compile-stub")
(use compile-stub)
(test-module 'compile-stub)

;; If you don't want `gosh' to exit with nonzero status even if
;; the test fails, pass #f to :exit-on-failure.
(test-end :exit-on-failure #t)
